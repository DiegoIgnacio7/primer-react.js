import React, { Component } from 'react';
import Location from './Location';
import './styles.css';
import { CLOUD, TSUNAMI, WINDY, METEOR, TORNADO } from './../../constants/weathers'
import WeatherData from './WeatherData/Index';
import convert from 'convert-units';

const api_key = '596abd2e613e125f423fc5a78292ffc6'
const location = "Santiago,cl"
const api_weather = `https://api.openweathermap.org/data/2.5/weather?q=${location},uk&APPID=${api_key}&units=metric`

const data1 = {
    temperature: 20,
    weatherState: CLOUD,
    humidity: 20,
    wind: '20 m/s'
};




class WeatherLocation extends Component {

    //TAMBIEN SE PUEDE HACER SIN CONSTRUCTOR
    // state = {
    //     data : data1,
    //     city : 'Santiago'
    // };

    constructor() {
        super();
        this.state = {
            data: data1,
            city: 'Santiago'
        };
    }


    getWeatherState = (weather) =>
        TSUNAMI;

    getTemperature=(temp) =>{
        return convert(temp).from('K').to('C').toFixed(2);
    };

    getData =(weather_Data) =>{
        const{humidity,temp} =weather_Data.main;
        const {speed} = weather_Data.wind;
        const weatherState = this.getWeatherState(this.weather);
        const temperature = this.getTemperature(temp);

        const data ={
            humidity : humidity, // se puede dejar como en weatherState si es el mismo nombre
            temperature :temp,
            weatherState,
            wind: `${speed} m/s`
        }

        return data;
    };

    handleUpdateClick = () => {
        fetch(api_weather).then(data => {
            
            return data.json();

        }).then(weather_data => {
            const dataaa = this.getData(weather_data);

            this.setState({data:dataaa})
            console.log(weather_data)
        });



    }


    render = () => {
        const { city, data } = this.state;

        return (<div className='weatherLocationCont'>
            <Location city={city} />
            <WeatherData data={data} />
            <button onClick={this.handleUpdateClick} >Actualizar</button>

        </div>
        )
    };
}

export default WeatherLocation; {/*Se exporta el nombre de la function arrow*/ }



