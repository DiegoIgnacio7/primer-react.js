import React from 'react';
import PropTypes from 'prop-types';
import WeaterTemperature from './WeatherTemperature';
import WeatherExtraInfo from './WeatherExtraInfo';
import {CLOUD, TSUNAMI, WINDY, METEOR, TORNADO } from '../../../constants/weathers'
import './styles.css';

const WeatherData = ({ data }) => {

    const {temperature, weatherState, humidity,wind} = data

    return(
<div className = 'weatherDataCont'>
    <WeaterTemperature temperature = {temperature} weatherState={weatherState} />
    <WeatherExtraInfo humidity={humidity} wind={wind}  />
</div>
)};

WeatherData.propTypes = {
    data: PropTypes.shape({
        temperature:PropTypes.string.isRequired,
        weatherState: PropTypes.string.isRequired,
        humidity: PropTypes.number.isRequired,
        wind: PropTypes.string.isRequired,

        
    }),
};

export default WeatherData;
