import React from 'react';
import WeatherIcons from 'react-weathericons';
import {CLOUD, TSUNAMI, WINDY, METEOR, TORNADO } from '../../../constants/weathers'
import PropTypes from 'prop-types';
import './styles.css';


const StateToIcon = (weatherState) => {
switch (weatherState) {
    case CLOUD:        
        return 'cloud';
    
    case TSUNAMI:
        return 'tsunami';

    case WINDY:
        return 'windy'

    case METEOR:
        return 'meteor'
    case TORNADO:
        return 'tornado'
    default:
    return 'alien';        
}
};


const getWeatherIcon = (weatherState) => {

    return <WeatherIcons className='wicon' name={StateToIcon(weatherState)} size='4x' />            
    
}


const WeatherTemperature = ({temperature, weatherState}) => (
<div className='WeatherTemperatureCont'>
{ getWeatherIcon(weatherState)}

    <span className='temperature'>{` ${temperature} C°`}</span>
</div>

);


WeatherTemperature.propTypes ={
    temperature : PropTypes.number.isRequired,
    weatherState : PropTypes.string,

};


export default WeatherTemperature;